
class: center, middle, inverse
#  TDD
## Test Driven DEVELOPMENT

.footnote[Michał Maryniak]

---
# O mnie
Michał Maryniak
[https://www.linkedin.com/in/michalmaryniak](https://www.linkedin.com/in/michalmaryniak)

- Praca
    - 1998 – Visual Basic for Applications - Word
    - Unizeto
    - Tieto
    - Cognitran

- Szkoła
    - MSc - Electronic Engineering and Telecommunications 1998-2003
    - MSc - Computer Science and Information Systems 2001-2005
    - PhD studies - Faculty of Mechanical Engineering 2003 - 2008
    - PhD thesis - Faculty of Technical Physics and Applied Mathematics
Technical University of Gdansk

---
class: center, middle, inverse
# O Was

--

## co umiecie?

--

## co chcecie umieć?

---
# Jak się nie uczyć / jak się uczyć
- pruska szkoła / 100zł
- zwolnij / zwolnijmy!
- ćwiczenia / notatki
- pytaj !
- przerwy (wołajcie kiedy czas na przerwę)
- rozmawiajmy na głos
- słuchaj mózgu swego
- poczuj ...
- zrozum ... znajdź ... nie zapamiętasz tego ...
- eksperymentuj! również w domu!
- ćwicz ćwicz ćwicz ...
- chciej ;-)

---
# Agenda
- Testowanie
 - Dlaczego testujemy oprogramowanie?
 - Rodzaje testów
 - Testy jednostkowe

- Programowanie zwinne (Agile Software Development)
- Extreme programming
- TDD
- Biblioteki do testów
 - JUnit
 - Mockito
 - Assertions

- Dobre praktyki
- Złe wzorce

---
class: center, middle, inverse
# Dlaczego testujemy?

---
.image-full[![testy](https://memegenerator.net/img/instances/82173311/i-ja-mu-wtedy-mwi-testy-a-co-ja-w-gimnazjum-jestem.jpg)]

---
# Dlaczego testujemy?
- Błędy programistów
- Błędy wymagań
- Nieprzewidziane scenariusze
- Bezpieczeństwo ( audyty )
- Współpraca / integracja aplikacji
- Obciążenie aplikacji

---
# Spektaularne wpadki
- Pentium
 - Profesor matematyki: Pentiumy nie potrafią liczyć! 500 mln $
- Rakieta Ariane
 - 10 lat 7 mld $ Integer overflow  - kod z poprzedniej wersji
- System Patriot
 - Zatoka Perska 91 - 28 ofiar, 100 rannych, sekunda != sekunda
- Mariner 1
 - „najdroższy myślniki w historii”
- Bład architektury procesorów
 - memory leak,  spadek 5-30% wydajności

---
# Testowanie
Testowanie ma na celu weryfikację oraz walidację oprogramowania.

Weryfikacja oprogramowania pozwala skontrolować, czy wytwarzane oprogramowanie jest zgodne ze specyfikacją.

Walidacja
sprawdza, czy oprogramowanie jest zgodne z oczekiwaniami użytkownika.

Wymagania funkcjonale i niefunkcjonalne.

---
#Podział testów

## Testy dzielimy na
- statyczne / dynamiczne
- biała skrzynka / czarna skrzynka

--

## Pozmiomy testowania
- testy jednostkowe
- testy integracyjne komponentów ("big bang")
- testy systemowe (całość)
- testy akceptacyjne
- testy integracyjne z innymi systemami

---
## A także
 - Sanity tests ( czy cokolwiek działa zgodnie z rozumem )
 - Smoke tests ( najbardziej kluczowe funkcjonalności )
 - Regression tests

---
class: center, middle, inverse
# „Testowanie  może  wykazać  wyłącznie  istnienie bugów, nigdy ich brak.”
Edsger W. Dijkstra

---
# Testy jednostkowe
Testowany fragment programu poddawany jest testowi,
który wykonuje go i porównuje wynik
z oczekiwanymi wynikami.

Zaletą testów jednostkowych jest możliwość wykonywania
na bieżąco w pełni zautomatyzowanych testów
na modyfikowanych elementach programu,
co umożliwia często wychwycenie błędu natychmiast po
jego pojawieniu się i szybką jego lokalizację zanim
dojdzie do wprowadzenia błędnego fragmentu do programu.

Testy jednostkowe są również formą specyfikacji.

---
#Budzik
Przygotuj test dla klasy Budzik zwracającej godzinę pobudki w zależności od dnia tygodnia:
PON-PI - 6
SOB-NIEDZ - 10

---
#Rozwiązanie
- naiwne
- @Test
- @AssertEquals


---
#JUnit
Adnotacja | Znaczenie
------|--------
@Test | Identifies a method as a test method.
@Before |  Executed before each test. It is used to prepare the test environment (e.g., read input data, initialize the class).
@After | Executed after each test. It is used to cleanup the test environment (e.g., delete temporary data, restore defaults). It can also save memory by cleaning up expensive memory structures.
@BeforeClass | Executed once, before the start of all tests. It is used to perform time intensive activities, for example, to connect to a database. Methods marked with this annotation need to be defined as static to work with JUnit.
@AfterClass | Executed once, after all tests have been finished. It is used to perform clean-up activities, for example, to disconnect from a database. Methods annotated with this annotation need to be defined as static to work with JUnit.

---
#JUnit
Metoda | Znaczenie
------|--------
fail([message]) | Let the method fail. Might be used to check that a certain part of the code is not reached or to have a failing test before the test code is implemented. The message parameter is optional.
assertTrue([message,] boolean condition) | Checks that the boolean condition is true.
assertFalse([message,] boolean condition) | Checks that the boolean condition is false.
assertEquals([message,] expected, actual) | Tests that two values are the same. Note: for arrays the reference is checked not the content of the arrays.
assertEquals([message,] expected, actual, tolerance) | Test that float or double values match. The tolerance is the number of decimals which must be the same.

---
#JUnit
Metoda | Znaczenie
------|--------
assertNull([message,] object) | Checks that the object is null.
assertNotNull([message,] object) | Checks that the object is not null.
assertSame([message,] expected, actual) | Checks that both variables refer to the same object.
assertNotSame([message,] expected, actual) | Checks that both variables refer to different objects.


---
#Testy parametryzowane

@Parameterized.Parameter(value=0)
@Parameterized.Parameter(value=1)
@Parameterized.Parameters(name = "{index}: dnia {0} pobudka o {1}")

---
# Model kaskadowy
.image-full[![kaskada](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/POL_model_kaskadowy.svg/1024px-POL_model_kaskadowy.svg.png)]

---
class: center, middle, inverse
# Programowanie zwinne
## Agile Software Development

---
class: center, middle, inverse
# Agile Manifesto

Odkrywamy nowe metody programowania dzięki praktyce w programowaniu
i wspieraniu w nim innych.
W wyniku naszej pracy, zaczęliśmy bardziej cenić

--

 ## ludzie i interakcje > procesy i narzędzia
 ## działające oprogramowanie > obszerna dokumentację
 ## współpracę z klientem > formalne ustalenia
 ## reagowanie na zmiany > podążanie za planem

--

Oznacza to, że elementy wypisane po prawej są wartościowe,
ale większą wartość mają dla nas te, które wypisano po lewej

---
#Programowanie ekstremalne XP
- iteracyjność
- architektura się rozwija
- testy jednostkowe / TDD
- ciągła modifykacja architektury
- programowanie parami
- ciągły kontakt z klientem

---
class: center, middle, inverse
# TDD


---
#Tradycyjnie
- wymagania
- kod
- testy
- czy przechodzi testy?

--

#TDD
- wymagania
- testy
- kod
- czy przechodzi testy?

---
#TDD
 - Najpierw programista pisze automatyczny test sprawdzający dodawaną funkcjonalność. Test w tym momencie nie powinien się udać.
 - Później następuje implementacja funkcjonalności. W tym momencie wcześniej napisany test powinien się udać.
 - W ostatnim kroku programista dokonuje refaktoryzacji napisanego kodu, żeby spełniał on oczekiwane standardy.

## RED -> GREEN -> REFACTOR
 - TDD Mantra

 - Jak długie cykle?
 - Komituj tylko GREEN
 - Na poniedziałek zostaw RED ;-)


---
#Given / When / Then
- Given
- When
- Then


---
class: center, middle, inverse
#Refactoring
##„Pierwszy  lepszy  głupiec  potrafi  napisać  kod zrozumiały  dla  komputera.  Dobrzy  programiści piszą kod zrozumiały dla człowieka.”
Martin Fowler


---
# Kalkulator
``` java

    @Test
    public void testAdd() {
        //given
        SimpleCalculator sc = new SimpleCalculator();

        //when
        int res = sc.add(5, 6);

        //then
        assertEquals(11, res);

    }

```

---
class: center, middle, inverse
# Odwrotna notacja Polska


---
class: center, middle, inverse
# Notacje
    7 / (2 + 3)

## notacja Łukasiewicza - przedstawiona w 1920r.
    / 7 + 2 3

## odwrotna notacja Polska
### Azciweisakul notation
    7 2 3 + /

---
# Odwrotna notacja Polska
https://neil.fraser.name/software/hp-35/

---
# Stack

```java
Deque<String> stack = new ArrayDeque<>();
stack.push("raz");  //add first
stack.push("dwa");
stack.push("trzy");
show(stack);

System.out.println("pop: " + stack.pop());  //remove first
show(stack);

System.out.println("pop: " + stack.pop());
show(stack);
```

---
# Pierwsze podejście
```java
 PolishCalculator polishCalculator = new PolishCalculator();

    @Test(expected = IllegalStateException.class)
    public void test1() {
        int res = polishCalculator.getAccumulator();
    }

    @Test
    public void enterAndDropping() {
        polishCalculator.enter(1);
        polishCalculator.enter(2);
        polishCalculator.enter(3);

        assertEquals(3, polishCalculator.getAccumulator());
        polishCalculator.drop();

        assertEquals(2, polishCalculator.getAccumulator());
        polishCalculator.drop();
        assertEquals(1, polishCalculator.getAccumulator());

    }
```


---
# Operatory
```java
    PolishCalculator polishCalculator = new PolishCalculator();

    @Before
    public void setup() {
        polishCalculator.enter(6);
        polishCalculator.enter(9);
        polishCalculator.enter(3);
    }

    @Test
    public void testAddition() {
        polishCalculator.add();
        assertEquals(12, polishCalculator.getAccumulator());
    }

    @Test
    public void testAvg() {
        polishCalculator.avg(3);
        assertEquals(6, polishCalculator.getAccumulator());
    }
```

---
# Całość
```java
        @Test
        public void testSimple() {
            assertEquals(6, polishCalculator.calculate("4 2 +"));
            assertEquals(7, polishCalculator.calculate("4 3 +"));
            assertEquals(1, polishCalculator.calculate("4 3 -"));
            assertEquals(5, polishCalculator.calculate("4 3 + 2 -"));
        }

        @Test
        public void testHarderOne() {
            assertEquals(40, polishCalculator.calculate("12 2 3 4 * 10 5 / + * +"));
        }

        @Test
        public void testFromWiki() {
            assertEquals(1, polishCalculator.calculate("7 2 3 + /"));
        }

        @Test
        public void avgAndSum() {
            assertEquals(6, polishCalculator.calculate("123 6 3 9 avg_3"));
            assertEquals(12, polishCalculator.calculate("123 6 3 9 sum_2"));
        }
```

---
#Pomocnicy testowania
## Fake
## Stub
## Mock

---
# Fake
Fakes are objects that have working implementations, but not same as production one. Usually they take some shortcut and have simplified version of production code.

Baza "in memory"

---
# Stub
Stub is an object that holds predefined data and uses it to answer calls during tests. It is used when we cannot or don’t want to involve objects that would answer with real data or have undesirable side effects.

---
# Mock
Mocks are objects that register calls they receive.
In test assertion we can verify on Mocks that all expected actions were performed.
Email service.

---
class: center, middle, inverse
# User service

## User service
## User repository
## Time Source
## Audit Log


---
# UserService
- rejestruje używkownika
- podaje listę użytkowników
- pozwala znaleźć użytkownika po nazwie

--

```java
    public interface UserService {
        List<User> users();
        void register(String username);
        User find(String username);
    }
```

---
# UserStore
- findAll
- store
--

```java
    public interface UserStore {
        void store(User user);
        List<User> findAll();
    }
```

---
# TimeSource
- źródło czasu

--

```java
    public interface TimeSource {
        LocalDateTime currentTime();
    }
```

---
# AuditLog
- logujemy tu zdarzenia

--

```java
    public interface AuditLog {
        void log(String logType, String subtype, String data);
    }
```

---
class: center, middle, inverse

#Co jest czym?
##Fake / Stub / Mock


---
# TestEmptyUserService
```java
	@Test
	public void testDefaultUserServiceHasNoUsers() {
		assertEquals(0,userService.users().size());
	}

	@Test
	public void testFindingNonExistentUser() {
		assertNull(userService.find("bob"));
	}
```

---
# TestEmptyUserService
```java
    @Test
    public void testRegisteringSingleUser() {
        mockAuditLog.expect("user", "register", "bob");

        userService.register("bob");
        List<User> users = userService.users();

        assertEquals(1, users.size());
        assertEquals("bob", users.get(0).getUsername());
        assertEquals(stubTimeSource.currentTime(), users.get(0).getCreationTime());
        mockAuditLog.verify();
    }
```


---
class: center, middle, inverse
# Mockito

---
# Simple mocking
```java
    @Before
    public void createUserService() {
        mockAuditLog = mock(AuditLog.class);
        fakeUserStore = new FakeUserStore();
        stubTimeSource = mock(TimeSource.class);

        when(stubTimeSource.currentTime()).thenReturn(LocalDateTime.of(2011, 12, 25, 12, 0, 0, 0));
        userService = new SimpleUserService(mockAuditLog, fakeUserStore, stubTimeSource);
    }
```


---
# Run Mockito
```java
    @RunWith(MockitoJUnitRunner.class)
    public class MockitoUserServiceTestWithNoUser {

        @Before
        public void init() {
            MockitoAnnotations.initMocks(this);
        }
     }
```

---
# InjectMocks
```java
    @RunWith(MockitoJUnitRunner.class)
    public class MockitoInjectedAnotherUserServiceTest {

        @Mock
        AuditLog mockAuditLog;

        @Spy
        FakeUserStore fakeUserStore;

        @Mock
        TimeSource stubTimeSource;

        @InjectMocks
        AnotherUserService userService;
    }
```


---
class: center, middle, inverse
# Mock libraries

---
#Biblioteki mock
- Mockito
- Easymock
- Powermock

---
class: center, middle, inverse
# Assert libraries

---
# Biblioteki matcherów
- AssertJ
```java
assertThat(frodo.getName()).isEqualTo("Frodo");
```
- Hamcrest
```java
assertThat("chocolate chips", theBiscuit.getChocolateChipCount(), equalTo(10));
```

- JUnit Assert
```java
assertEquals(str1, str2);
```










