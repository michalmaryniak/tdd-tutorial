package mm;

import java.util.Stack;

public class OldPolishCalculator {

    private Stack<Integer> values = new Stack<>();

    private int accumulator = 0;

    public int getAccumulator() {
        return accumulator;
    }

    public void setAccumulator(int accumulator) {
        this.accumulator = accumulator;
    }

    public void enter() {
        values.push(accumulator);
    }

    public void drop() {
        accumulator = values.pop();
    }
}
