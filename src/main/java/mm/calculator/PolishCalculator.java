package mm.calculator;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;
import java.util.function.BiFunction;

public class PolishCalculator {

    private Deque<Integer> values = new ArrayDeque<>();

    public int getAccumulator() {
        if (values.isEmpty())
            throw new IllegalStateException("Not data");
        else
            return values.peek();
    }

    public void enter(int value) {
        values.push(value);
    }

    int drop() {
        return values.pop();
    }

    public void show() {
        System.out.println(">-----");
        for (Integer value : values) {
            System.out.println(value);
        }
        System.out.println("<-----");
    }

    private void performBiOperation(BiFunction<Integer, Integer, Integer> operation) {
        int el1 = drop();
        int el2 = drop();
        int result = operation.apply(el2, el1);
        enter(result);
    }

    public void add() {
        performBiOperation((x, y) -> x + y);
    }

    public void subtract() {
        performBiOperation((x, y) -> x - y);
    }

    public void multiple() {
        performBiOperation((x, y) -> x * y);
    }

    public void divide() {
        performBiOperation((x, y) -> x / y);
    }


    public void avg(int count) {
        sum(count);
        enter (drop() / count);
    }

    public void sum(int count) {
        int sum = 0;
        for (int i = 0; i < count; i++) {
            sum += drop();
        }
        enter (sum);
    }
}
