package mm.calculator;

public class PolishStringCalculator {


    public int calculate(String s) {
        PolishCalculator polishCalculator = new PolishCalculator();

        String[] items = s.split(" ");

        for (String item : items) {
            if (knowsOperator(item)) {
                doOperation(polishCalculator, item);
            } else {
                polishCalculator.enter(Integer.parseInt(item));
            }
        }

        return polishCalculator.getAccumulator();
    }

    private boolean knowsOperator(String item) {
        if (item.equals("+") || item.equals("-") || item.equals("*") || item.equals("/") || item.startsWith("sum_") || item.startsWith("avg_"))
            return true;
        else
            return false;
    }

    private void doOperation(PolishCalculator polishCalculator, String item) {
        if (item.equals("+"))
            polishCalculator.add();
        else if (item.equals("-"))
            polishCalculator.subtract();
        else if (item.equals("*"))
            polishCalculator.multiple();
        else if (item.equals("/"))
            polishCalculator.divide();
        else if (item.startsWith("sum_"))
            polishCalculator.sum(Integer.parseInt(item.substring(4)));
        else if (item.startsWith("avg_"))
            polishCalculator.avg(Integer.parseInt(item.substring(4)));
        else
            throw new IllegalArgumentException("Operator not recognized: " + item);

    }


}
