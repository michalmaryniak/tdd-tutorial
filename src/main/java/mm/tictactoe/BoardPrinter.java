package mm.tictactoe;

public class BoardPrinter {

    public String printBoard(Board board) {
        String res = "";
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                res += board.getValue(x, y);
            }
            res += "\n";
        }
        return res;
    }
}
