package mm.tictactoe;

import static mm.tictactoe.BoardValue.NONE;

public class Board {

    private BoardValue[][] values = new BoardValue[][]{
                    new BoardValue[]{NONE, NONE, NONE},
                    new BoardValue[]{NONE, NONE, NONE},
                    new BoardValue[]{NONE, NONE, NONE}
    };

    private int itemsOnBoardCounter;

    private void validateRange(int x, int y) {
        if (x < 0 || x >= 3)
            throw new IllegalArgumentException("Illegal argument, wrong x range");
        if (y < 0 || y >= 3)
            throw new IllegalArgumentException("Illegal argument, wrong y range");
    }

    public BoardValue getValue(int x, int y) {
        validateRange(x, y);

        return values[x][y];
    }

    public void move(int x, int y, BoardValue m) {
        validateRange(x, y);

        if (m != BoardValue.X && m != BoardValue.O)
            throw new IllegalArgumentException("Illegal symbol");

        if (values[x][y] != NONE)
            throw new IllegalArgumentException("Illegal move, field is not empty");

        values[x][y] = m;
        itemsOnBoardCounter++;
    }

    public BoardValue giveWinner() {

        BoardValue[] candidates = new BoardValue[]{ BoardValue.O, BoardValue.X };

        for (BoardValue candidate : candidates) {
            for (int i = 0; i < 3; i++) {
                if (winnerInARow(i, candidate))
                    return candidate;
            }
            for (int i = 0; i < 3; i++) {
                if (winnerInAColumn(i, candidate))
                    return candidate;
            }

            if (winnerOnDiagonal(candidate))
                return candidate;
            if (winnerOnDiagonal2(candidate))
                return candidate;
        }

        return NONE;
    }

    private boolean winnerInARow(int i, BoardValue candidate) {
        for (int j = 0; j < 3; j++) {
            if (getValue(i, j) != candidate)
                return false;
        }
        return true;
    }

    private boolean winnerInAColumn(int i, BoardValue candidate) {
        for (int j = 0; j < 3; j++) {
            if (getValue(j, i) != candidate)
                return false;
        }
        return true;
    }

    private boolean winnerOnDiagonal(BoardValue candidate) {
        for (int j = 0; j < 3; j++) {
            if (getValue(j, j) != candidate)
                return false;
        }
        return true;
    }

    private boolean winnerOnDiagonal2(BoardValue candidate) {
        for (int j = 0; j < 3; j++) {
            if (getValue(j, 2 - j) != candidate)
                return false;
        }
        return true;
    }

    public boolean hasEmptySpace() {
        return itemsOnBoardCounter < 9;
    }

}
