package mm.tictactoe;

public enum BoardValue {
    NONE, X, O;

    @Override
    public String toString() {
        if(this == NONE)
            return "_";
        else
            return super.toString();
    }
}
