package mm.tictactoe;

import java.util.Scanner;

public class GameMain {

    public static void main(String[] args) {
        Board b = new Board();
        Scanner scanner = new Scanner(System.in);

        Player p1 = new Player(scanner, BoardValue.X, b);
        Player p2 = new Player(scanner, BoardValue.O, b);

        Game g = new Game(p1, p2, b, new BoardPrinter());
        Player winner = g.startGame();
    }
}
