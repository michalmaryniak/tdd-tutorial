package mm.tictactoe;

public class Game
{

    private Player p1;

    private Player p2;

    private Board board;

    private BoardPrinter boardPrinter;

    private Player currentPlayer;

    public Game(Player p1, Player p2, Board board, BoardPrinter boardPrinter)
    {
        this.p1 = p1;
        this.p2 = p2;
        this.board = board;
        this.boardPrinter = boardPrinter;
    }

    public Player startGame()
    {
        currentPlayer = p1;

        while (board.hasEmptySpace())
        {
            System.out.println(boardPrinter.printBoard(board));
            System.out.println("Ruch gracza:" + currentPlayer.getSymbol());

            letPlayerMakeMove();

            if (board.giveWinner() != BoardValue.NONE)
            {
                System.out.println("Wygrał: " + currentPlayer.getSymbol());
                System.out.println(boardPrinter.printBoard(board));
                return currentPlayer;
            }

            nextPlayer();
        }

        System.out.println("Remis");
        System.out.println(boardPrinter.printBoard(board));
        return null;    //remis, no winner

    }

    private void letPlayerMakeMove()
    {
        boolean correctMoveDone = false;
        while (!correctMoveDone)
        {
            try
            {
                currentPlayer.makeMove();
                correctMoveDone = true;
            }
            catch (IllegalArgumentException e)
            {
                System.out.println("Nieprawidlowy ruch (" + e.getMessage() + "), popraw ruch!");
            }
            catch (Exception e)
            {
                System.out.println("Jakis blad (" + e.getMessage() + "), popraw ruch!");
            }
        }
    }

    private void nextPlayer()
    {
        if (currentPlayer == p1)
        {
            currentPlayer = p2;
        }
        else if (currentPlayer == p2)
        {
            currentPlayer = p1;
        }
    }
}
