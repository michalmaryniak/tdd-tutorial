package mm.tictactoe;

import java.util.Scanner;

public class Player {

    private Scanner scanner;
    private BoardValue symbol;
    private Board board;

    public Player(Scanner scanner, BoardValue symbol, Board board) {
        this.scanner = scanner;
        this.symbol = symbol;
        this.board = board;
    }

    public void makeMove() {
        int x = scanner.nextInt();
        int y = scanner.nextInt();

        board.move(x - 1, y - 1, symbol);
    }

    public BoardValue getSymbol() {
        return symbol;
    }
}
