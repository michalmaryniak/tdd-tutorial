package mm.budzik;

public class Budzik {

    public int obudzMnie(Dzien dzien) {
        if(dzien == null)
            throw new IllegalArgumentException("Dzien cannot be null");

        int godzina = 6;
        if (dzien == Dzien.SOB || dzien == Dzien.NIEDZ)
            godzina = 10;


        System.out.println("Budzenie w dzien: " + dzien + " : o godzinie: " + godzina);
        return godzina;
    }


}
