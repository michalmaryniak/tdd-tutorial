package mm.users;

import java.util.List;

public interface UserService {

	List<User> users();

	void register(String username);

	User find(String username);

}