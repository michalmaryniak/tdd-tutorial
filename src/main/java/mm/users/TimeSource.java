package mm.users;


import java.time.LocalDateTime;

public interface TimeSource {
	LocalDateTime currentTime();
}
