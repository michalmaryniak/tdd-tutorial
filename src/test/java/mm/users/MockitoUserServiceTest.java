package mm.users;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

public class MockitoUserServiceTest {

    private UserService userService;
    private AuditLog mockAuditLog;
    private FakeUserStore fakeUserStore;
    private TimeSource stubTimeSource;

    @Before
    public void createUserService() {
        mockAuditLog = mock(AuditLog.class);
        fakeUserStore = spy(new FakeUserStore());
//        fakeUserStore = new FakeUserStore();
        stubTimeSource = mock(TimeSource.class);

        when(stubTimeSource.currentTime()).thenReturn(LocalDateTime.of(2011, 12, 25, 12, 0, 0, 0));

        userService = new SimpleUserService(mockAuditLog, fakeUserStore, stubTimeSource);

    }

    @Test
    public void testDefaultUserServiceHasNoUsers() {
        assertEquals(0, userService.users().size());
    }

    @Test
    public void testFindingNonExistentUser() {
        assertNull(userService.find("bob"));
    }

    @Test
    public void testRegisteringSingleUserProcess() {
        //given

        //when
        userService.register("bob");

        //then
        verify(mockAuditLog).log("user", "register", "bob");
    }

    @Test
    public void testRegisteringDuplicateUserProcess() {
        //given

        //when
        userService.register("bob");
        userService.register("bob");

        //then
        verify(mockAuditLog).log("user", "register", "bob");
        verify(mockAuditLog).log("user", "duplicateregister", "bob");
    }


    @Test
    public void testExistsSingleUser() {
        //given
        userService.register("bob");

        //when
        List<User> users = userService.users();

        assertEquals(1, users.size());
        assertEquals("bob", users.get(0).getUsername());
        assertEquals(stubTimeSource.currentTime(), users.get(0).getCreationTime());
    }


    @Test
    public void testExistTwoUsers() {
        //given
        userService.register("bob");
        userService.register("alice");

        //when
        List<User> users = userService.users();

        //then
        assertEquals(2, users.size());

        assertEquals("bob", users.get(0).getUsername());
        assertEquals(stubTimeSource.currentTime(), users.get(0).getCreationTime());

        assertEquals("alice", users.get(1).getUsername());
        assertEquals(stubTimeSource.currentTime(), users.get(1).getCreationTime());
    }

    @Test
    public void testDoesNotRegisterDuplicateUser() {
        //given
        userService.register("bob");
        userService.register("bob");

        //when
        List<User> users = userService.users();

        //then
        assertEquals(1, users.size());

        assertEquals("bob", users.get(0).getUsername());
        assertEquals(stubTimeSource.currentTime(), users.get(0).getCreationTime());
    }

    @Test
    public void testFindUserByName() {
        //given
        userService.register("bob");
        userService.register("alice");

        //when
        User user1 = userService.find("bob");
        User user2 = userService.find("alice");

        //then
        assertEquals("bob", user1.getUsername());
        assertEquals("alice", user2.getUsername());
    }

    @Test
    public void testDoesNotFindUserByWrongName() {
        //given
        userService.register("bob");
        userService.register("alice");

        //when
        User user1 = userService.find("Bob");
        User user2 = userService.find("ALLA");

        //then
        assertNull("Bob should not be found", user1);
        assertNull("There should be no ALLA", user2);
    }

    @Test
    public void testDoesCallRepo() {
        //given

        //when
        userService.register("bob");

        //then
        User user = new User("bob", stubTimeSource.currentTime());
        verify(fakeUserStore).store(user);
    }

    @Test
    public void testDoesCallRepoWithCaptor() {
        //given
        ArgumentCaptor<User> argUser = ArgumentCaptor.forClass(User.class);
        
        //when
        userService.register("bob");
        userService.register("alice");

        //then
        
        verify(fakeUserStore, times(2)).store(argUser.capture());

        List<User> usersCaptured = argUser.getAllValues();
        assertEquals("bob", usersCaptured.get(0).getUsername());
        assertEquals("alice", usersCaptured.get(1).getUsername());
    }


}
