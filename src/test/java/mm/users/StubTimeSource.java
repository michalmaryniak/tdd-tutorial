package mm.users;


import java.time.LocalDateTime;

public class StubTimeSource implements TimeSource {

	@Override
	public LocalDateTime currentTime() {
		return LocalDateTime.of(2011, 12, 25, 12, 0, 0, 0);
	}

}
