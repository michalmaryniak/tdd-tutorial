package mm.budzik;

public class BudzikTest
{
    public static void main(String[] args) {

        Budzik budzik = new Budzik();

        Dzien[] dni = {Dzien.PON, Dzien.WT, Dzien.SR, Dzien.CZW, Dzien.PIAT, Dzien.SOB, Dzien.NIEDZ};
        int[] oczekiwanePobudki = {6, 6, 6, 6, 6, 10, 10};

        for (int i = 0; i < 7; i++)
        {
            int oczekiwanaPobudka = oczekiwanePobudki[i];
            int budzikZadzwonilOGodzinie = budzik.obudzMnie(dni[i]);

            if(budzikZadzwonilOGodzinie != oczekiwanaPobudka)
                System.out.println(String.format("Budzik mnie obudził o %s a miałem wstać o %s!", budzikZadzwonilOGodzinie, oczekiwanaPobudka));
        }
    }

}
