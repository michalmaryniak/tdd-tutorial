package mm.budzik;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class BudzikTest3 {

    Budzik budzik = new Budzik();

    @Parameterized.Parameter(value=0)
    public Dzien dzien;

    @Parameterized.Parameter(value=1)
    public int oczekiwanaPobudka;

    @Parameterized.Parameters(name = "{index}: dnia {0} pobudka o {1}")
    public static Collection<Object[]> parameters() {
        List<Object[]> list = new ArrayList<>();

        list.add(new Object[]{Dzien.PON, 6});
        list.add(new Object[]{Dzien.WT, 6});
        list.add(new Object[]{Dzien.SR, 6});
        list.add(new Object[]{Dzien.CZW, 6});
        list.add(new Object[]{Dzien.PIAT, 6});
        list.add(new Object[]{Dzien.SOB, 10});
        list.add(new Object[]{Dzien.NIEDZ, 10});

        return list;
    }

    @Test
    public void testDniaPowszedniego() {
        int budzikZadzwonilOGodzinie = budzik.obudzMnie(dzien);
        assertEquals("Budzik powinien zadzwonić o określonej porze", oczekiwanaPobudka, budzikZadzwonilOGodzinie);
    }

}
