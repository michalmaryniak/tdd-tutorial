package mm.budzik;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BudzikTest2
{
    Budzik budzik = new Budzik();

    @Test
    public void testDniaPowszedniego() {
        Dzien[] dni = {Dzien.PON, Dzien.WT, Dzien.SR, Dzien.CZW, Dzien.PIAT};
        int oczekiwanaPobudka = 6;

        for (Dzien dzien : dni) {
            int budzikZadzwonilOGodzinie = budzik.obudzMnie(dzien);

            assertEquals("Budzik powinien zadzwonić o określonej porze", oczekiwanaPobudka, budzikZadzwonilOGodzinie);
        }
    }

    @Test
    public void testDniaSwiatecznego() {
        Dzien[] dni = {Dzien.SOB, Dzien.NIEDZ};
        int oczekiwanaPobudka = 10;

        for (Dzien dzien : dni) {
            int budzikZadzwonilOGodzinie = budzik.obudzMnie(dzien);

            assertEquals("Budzik powinien zadzwonić o określonej porze", oczekiwanaPobudka, budzikZadzwonilOGodzinie);
        }
    }

}
