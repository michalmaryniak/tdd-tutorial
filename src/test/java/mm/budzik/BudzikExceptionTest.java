package mm.budzik;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class BudzikExceptionTest {

    Budzik budzik = new Budzik();

    @Test(expected = IllegalArgumentException.class)
    public void testFailure() {
        int budzikZadzwonilOGodzinie = budzik.obudzMnie(null);
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testFailure2() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Dzien cannot be null");
        int budzikZadzwonilOGodzinie = budzik.obudzMnie(null);
        Assert.fail("No way...");
    }
}
