package mm.calculator;

import mm.calculator.PolishCalculator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PolishCalculatorTest {

    PolishCalculator polishCalculator = new PolishCalculator();

    @Test(expected = IllegalStateException.class)
    public void test1() {
        int res = polishCalculator.getAccumulator();
    }

    @Test
    public void enterAndDropping() {
        polishCalculator.enter(1);
        polishCalculator.enter(2);
        polishCalculator.enter(3);

        assertEquals(3, polishCalculator.getAccumulator());
        polishCalculator.drop();

        assertEquals(2, polishCalculator.getAccumulator());
        polishCalculator.drop();
        assertEquals(1, polishCalculator.getAccumulator());

    }


}
