package mm.calculator;

import mm.calculator.PolishCalculator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PolishStringCalculatorOperations {

    PolishStringCalculator polishCalculator = new PolishStringCalculator();

    @Test
    public void testAddition() {
        assertEquals(6, polishCalculator.calculate("4 2 +"));
    }

    @Test
    public void testAnotherAddition() {
        assertEquals(7, polishCalculator.calculate("4 3 +"));
    }

    @Test
    public void testSubstract() {
        assertEquals(1, polishCalculator.calculate("4 3 -"));
    }

    @Test
    public void testAdditioAndSubstract() {
        assertEquals(5, polishCalculator.calculate("4 3 + 2 -"));
    }

    @Test
    public void testHarderOne() {
        assertEquals(40, polishCalculator.calculate("12 2 3 4 * 10 5 / + * +"));
    }

    @Test
    public void testFromWiki() {
        assertEquals(1, polishCalculator.calculate("7 2 3 + /"));
    }

    @Test
    public void avg3() {
        assertEquals(6, polishCalculator.calculate("123 6 3 9 avg_3"));
    }

    @Test
    public void sum2() {
        assertEquals(12, polishCalculator.calculate("123 6 3 9 sum_2"));
    }

}
