package mm.calculator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PolishCalculatorOperationsMultipleTest {

    PolishCalculator polishCalculator = new PolishCalculator();

    @Before
    public void setup() {
        polishCalculator.enter(9);
        polishCalculator.enter(3);
        polishCalculator.enter(6);
    }

    @Test
    public void testAvg() {
        polishCalculator.avg(3);
        assertEquals(6, polishCalculator.getAccumulator());
    }

    @Test
    public void testSum3() {
        polishCalculator.sum(3);
        assertEquals(18, polishCalculator.getAccumulator());
    }

    @Test
    public void testSum2() {
        polishCalculator.sum(2);
        assertEquals(9, polishCalculator.getAccumulator());
    }

}
