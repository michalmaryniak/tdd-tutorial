package mm.calculator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PolishCalculatorOperationsTest {

    PolishCalculator polishCalculator = new PolishCalculator();

    @Before
    public void setup() {
        polishCalculator.enter(9);
        polishCalculator.enter(3);
    }

    @Test
    public void testAddition() {
        polishCalculator.add();
        assertEquals(12, polishCalculator.getAccumulator());
    }

    @Test
    public void testSubstract() {
        polishCalculator.subtract();
        assertEquals(6, polishCalculator.getAccumulator());
    }

    @Test
    public void testMultiple() {
        polishCalculator.multiple();
        assertEquals(27, polishCalculator.getAccumulator());
    }

    @Test
    public void testDivide() {
        polishCalculator.divide();
        assertEquals(3, polishCalculator.getAccumulator());
    }

}
