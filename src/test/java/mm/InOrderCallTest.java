package mm;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

public class InOrderCallTest
{
    @Test
    public void inOTest() {

        InOrderCall mock1 = mock(InOrderCall.class);

        mock1.do2();
        mock1.do1();

        verify(mock1).do1();
        verify(mock1).do2();
    }

    @Test
    public void inOTest2() {
        InOrderCall mock1 = mock(InOrderCall.class);

        InOrder inOrder = Mockito.inOrder(mock1);

        mock1.do1();
        mock1.do2();

        inOrder.verify(mock1).do1();
        inOrder.verify(mock1).do2();
    }

    @Test
    public void inOTest3() {
        InOrderCall mock1 = mock(InOrderCall.class);
        InOrderCall mock2 = mock(InOrderCall.class);

        InOrder inOrder = Mockito.inOrder(mock2, mock1);

        mock1.do1();
        mock1.do2();

        mock2.do1();
        mock2.do2();


        inOrder.verify(mock1).do1();
        inOrder.verify(mock1).do2();
        inOrder.verify(mock2).do1();
        inOrder.verify(mock2).do2();
    }


    @Test
    public void inOTest4() {
        InOrderCall mock1 = mock(InOrderCall.class);
        InOrderCall mock2 = mock(InOrderCall.class);

        InOrder inOrder = Mockito.inOrder(mock1, mock2);

        mock1.do1();
        mock2.do1();

        inOrder.verify(mock1).do1();
        inOrder.verify(mock2).do1();

    }

}
