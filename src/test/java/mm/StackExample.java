package mm;

import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;

public class StackExample {

    private void show(Queue<String> stack) {

        for (String s : stack) {
            System.out.println("|" + s + "\t|");
        }
        System.out.println("---------");
    }

    @Test
    public void stack() {
        Deque<String> stack = new ArrayDeque<>();
        stack.push("raz");
        stack.push("dwa");
        stack.push("trzy");
        show(stack);

        System.out.println("pop: " + stack.pop());
        show(stack);

        System.out.println("pop: " + stack.pop());
        show(stack);

        System.out.println("peek: " + stack.peek());
        show(stack);

        System.out.println("pop: " + stack.pop());
        show(stack);

    }
}
