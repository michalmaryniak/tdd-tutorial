package mm.simplecalc;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class SimpleCalculatorTest {

    @Test
    public void testAdd() {
        //given
        SimpleCalculator sc = new SimpleCalculator();

        //when
        int res = sc.add(5, 6);

        //then
        assertEquals(11, res);

    }
}
