package mm.tictactoe;

import org.junit.Test;

import static org.junit.Assert.*;

public class BoardTest {

    Board board = new Board();

//  - sprawdzający, czy da się utworzyć nową plansze
//	- czy nowa plansza zawiera puste wartości
    @Test
    public void testNewBoard() {
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                BoardValue v1 = board.getValue(x, y);
                assertEquals(BoardValue.NONE, v1);
            }
        }
    }

    //	- czy da się dodać prawidłowy ruch
    //	- czy da się pobrać element planszy o prawidłowym adresie
    @Test
    public void testNewMove() {
        board.move(0, 2, BoardValue.X);
        assertEquals(BoardValue.X, board.getValue(0, 2));

        board.move(1, 1, BoardValue.O);
        assertEquals(BoardValue.O, board.getValue(1, 1));

        assertEquals(BoardValue.NONE, board.getValue(1, 2));
    }

    @Test
    public void testNewMoveX() {
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                board.move(x, y, BoardValue.X);
                BoardValue v1 = board.getValue(x, y);
                assertEquals(BoardValue.X, v1);
            }
        }
    }

    @Test
    public void testNewMoveO() {
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                board.move(x, y, BoardValue.O);
                BoardValue v1 = board.getValue(x, y);
                assertEquals(BoardValue.O, v1);
            }
        }
    }

    //	- czy da się dodać dwa razy taki sam ruch
    @Test
    public void testMoveTwice() {
        board.move(0, 1, BoardValue.O);
        try {
            board.move(0, 1, BoardValue.X);
            fail("should not pass");
        } catch (IllegalArgumentException e) {
        }
    }

    //	- czy da się dodać nieprawidłowy ruch
    //	(rozważyć przypadki podania niepoprawnych koordynatów oraz niepoprawnego symbolu)
    @Test(expected = IllegalArgumentException.class)
    public void testIllegalRange() {
        board.move(-2, 1, BoardValue.O);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalRange2() {
        board.move(2, 3, BoardValue.X);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalSymbol() {
        board.move(2, 1, BoardValue.NONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalSymbol2() {
        board.move(1, 1, null);
    }

//	- czy da się pobrać element planszy o nieprawidłowym adresie
    @Test(expected = IllegalArgumentException.class)
    public void testGetIllegalRange() {
        board.getValue(-2, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetIllegalRange2() {
        board.getValue(2, 3);
    }


    //test zapenienia tablicy
    @Test
    public void testFullBoard() {
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                assertTrue(board.hasEmptySpace());
                board.move(x, y, BoardValue.X);
            }
        }
        assertFalse(board.hasEmptySpace());
    }
}
