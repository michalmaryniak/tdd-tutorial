package mm.tictactoe;

import org.junit.Test;

import static org.junit.Assert.*;

public class BoardPrinterTest {
//
//    6. Klasę, która wyświetla planszę
//	- jak wyświetla planszę, kiedy jest pusta
//	- jak wyświetla planszę, kiedy jest całkowicie zapełniona
//	- jak wyświetla planszę, kiedy zawiera kilka ustawionych pól

    Board b = new Board();
    BoardPrinter boardPrinter = new BoardPrinter();

    @Test
    public void testPrint() {
        b.move(0, 1, BoardValue.X);
        b.move(0, 2, BoardValue.O);
        b.move(1, 2, BoardValue.X);
        b.move(2, 0, BoardValue.X);

        System.out.println(boardPrinter.printBoard(b));
        
        assertEquals("__X\nX__\nOX_\n", boardPrinter.printBoard(b));
    }
}