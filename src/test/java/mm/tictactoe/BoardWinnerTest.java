package mm.tictactoe;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BoardWinnerTest<pb>
{
//    2. Klasę sprawdzającą, czy dala plansza jest już rozwiązana (czy któryś z graczy wygrał). Testy:
//            - czy dla pustej planszy metoda sprawdzająca zwraca informacje o tym, że nikt nie wygrał

    Board b = new Board();

    BoardPrinter pb = new BoardPrinter();

    @Test
    public void testNoWinnerOnEmptyBoard() {
        assertEquals(BoardValue.NONE, b.giveWinner());
    }

    @Test
    public void testNoWinnerOnSomeBoard() {
        b.move(0, 1, BoardValue.O);
        b.move(1, 1, BoardValue.X);
        b.move(0, 2, BoardValue.O);
        b.move(2, 1, BoardValue.X);

//        ___
//        OXX
//        O__

        assertEquals(BoardValue.NONE, b.giveWinner());
    }


//	- czy jeżeli w którymkolwiek z wierszy są 3 symbole jednego gracza, to czy zwracany jest symbol tego gracza

    @Test
    public void testWinnerInAColumn() {
        b.move(0, 1, BoardValue.O);
        b.move(1, 1, BoardValue.X);
        b.move(0, 2, BoardValue.O);
        b.move(2, 1, BoardValue.X);
        b.move(0, 0, BoardValue.O);

//      O__
//      OXX
//      O__

        assertEquals(BoardValue.O, b.giveWinner());
    }

    @Test
    public void testWinnerInARow() {
        b.move(0, 2, BoardValue.O);
        b.move(1, 1, BoardValue.X);
        b.move(1, 2, BoardValue.O);
        b.move(2, 1, BoardValue.X);
        b.move(2, 2, BoardValue.O);

//        ___
//        _XX
//        OOO

        assertEquals(BoardValue.O, b.giveWinner());
    }

    @Test
    public void testWinnerOnDiag1() {
        b.move(0, 0, BoardValue.X);
        b.move(1, 1, BoardValue.X);
        b.move(2, 2, BoardValue.X);
        b.move(2, 1, BoardValue.O);
        b.move(1, 2, BoardValue.O);

//        X__
//        _XO
//        _OX

        assertEquals(BoardValue.X, b.giveWinner());
    }

    @Test
    public void testWinnerOnDiag2() {
        b.move(2, 0, BoardValue.X);
        b.move(1, 1, BoardValue.X);
        b.move(0, 2, BoardValue.X);
        b.move(2, 1, BoardValue.O);
        b.move(1, 2, BoardValue.O);

//        __X
//        _XO
//        XO_

        assertEquals(BoardValue.X, b.giveWinner());
    }

//	- czy jeżeli w którejkolwiek z kolumn są 3 symbole jednego gracza, to czy zwracany jest symbol tego gracza
//	- czy jeżeli na którejkolwiek przekątnej są 3 symbole jednego gracza, to czy zwracany jest symbol tego gracza

}
