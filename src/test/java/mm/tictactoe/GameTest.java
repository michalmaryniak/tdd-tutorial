package mm.tictactoe;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

@RunWith(MockitoJUnitRunner.class)
public class GameTest
{
//    5. Klasę zarządzającą grą
//	- sprawdzić, czy da się utworzyć taką klasę
//	(rozważyć, czy powinna ona mieć konstruktor bezparametrowy, czy może powinna wymagać jakiejś wartości)

    @Mock
    Player p1;

    @Mock
    Player p2;

    @Mock
    Board board;

    Game g;

    int gameIterator;

    @Before
    public void init()
    {
        g = new Game(p1, p2, board, new BoardPrinter());
    }

    @Test
    public void testSimpleGame()
    {
        //given
        when(board.giveWinner()).thenReturn(BoardValue.X);
        when(board.hasEmptySpace()).thenReturn(true);

        //when
        Player winner = g.startGame();

        //then
        verify(p1).makeMove();
        assertEquals(p1, winner);
    }

    @Test
    public void testSimpleGame2()
    {
        //given
        when(board.hasEmptySpace()).thenReturn(true);

        BoardValue[] values = new BoardValue[]{BoardValue.NONE, BoardValue.X};
        when(board.giveWinner()).thenAnswer((inv) -> values[gameIterator++]);

        //when
        Player winner = g.startGame();

        //then
        verify(p1).makeMove();
        verify(p2).makeMove();

        assertEquals(p2, winner);
    }

    @Test
    public void testSimpleGame3()
    {
        InOrder inOrder = Mockito.inOrder(p1, p2);

        //given
        when(board.hasEmptySpace()).thenReturn(true);

        BoardValue[] values = new BoardValue[]{BoardValue.NONE, BoardValue.X};
        when(board.giveWinner()).thenAnswer((inv) -> values[gameIterator++]);

        //when
        Player winner = g.startGame();

        //then
        inOrder.verify(p1).makeMove();
        inOrder.verify(p2).makeMove();

        assertEquals(p2, winner);
    }


    @Test
    public void testRemis()
    {
        //given
        // udajemy, że plansza już jest pełna, czyli powoduje to remis
        when(board.hasEmptySpace()).thenReturn(false);

        //when
        Player winner = g.startGame();

        //then
        assertEquals("Spodziewamy się, że remis da zwycięzcę jako null", null, winner);
    }

    //	- sprawdzić, czy pobiera ruch od gracza
    //	- sprawdzić, czy pobiera ruchy od graczy naprzemiennie
    @Test
    public void testGame()
    {
        //given
        InOrder inOrder = Mockito.inOrder(p1, p2);

        BoardValue[] returns = new BoardValue[]{BoardValue.NONE, BoardValue.NONE, BoardValue.X};
        Answer<BoardValue> answer = invocation -> returns[gameIterator++];

        when(board.giveWinner()).then(answer);

        when(board.hasEmptySpace()).thenReturn(true);

        //when
        g.startGame();

        //then
        inOrder.verify(p1).makeMove();
        inOrder.verify(p2).makeMove();
    }

    //	- sprawdzić, czy przestaje pobierać ruch od gracza,
//	gdy gra zostanie zakończona (wygrana jednego z graczy lub przez remis)
    @Test
    public void testFinishGame()
    {
        //given
        InOrder inOrder = Mockito.inOrder(p1, p2);
        when(p1.getSymbol()).thenReturn(BoardValue.X);
        when(p2.getSymbol()).thenReturn(BoardValue.O);

        BoardValue[] returns = new BoardValue[]{BoardValue.NONE, BoardValue.NONE, BoardValue.X};
        Answer<BoardValue> answer = invocation -> returns[gameIterator++];
        when(board.giveWinner()).then(answer);

        when(board.hasEmptySpace()).thenReturn(true);

        //when
        Player winner = g.startGame();

        //then
        inOrder.verify(p1).makeMove();
        inOrder.verify(p2).makeMove();
        inOrder.verify(p1).makeMove();

        assertEquals(p1, winner);
    }

//	- sprawdzic, czy przekazuje pobrane od gracza koordynaty ruchu do planszy
//	- sprawdzic, czy przekazuje do planszy prawidłowy symbol gracza
//	- sprawdzić, czy po wykonaniu ruchu waliduje wygraną któregoś z graczy
//	- sprawdzić, jak reaguje kiedy walidator zwróci symbol jednego z graczy
//	- sprawdzić, jak reaguje, kiedy walidator zwróci pustą wartość (zero)
//	- sprawdzić, jak reaguje, kiedy skończą się wolne pola

}
