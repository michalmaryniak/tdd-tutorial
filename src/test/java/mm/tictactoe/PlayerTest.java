package mm.tictactoe;

import static org.mockito.Mockito.verify;

import java.util.Scanner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class PlayerTest {

//    3. Klasę gracza ludzkiego
//	- sprawdzić, czy da się utworzyć taką klasę
//	(rozważyć, czy powinna ona mieć konstruktor bezparametrowy, czy może powinna wymagać jakiejś wartości)
//	- czy po wpisaniu wartosci na strumien wejsciowy jest zwracany poprawny adres komórki planszy

    @Mock
    Board boardMock;

    @Test
    public void testPlayerMove() {
        //given
        Scanner scanner = new Scanner("1 2\n");
        Player player = new Player(scanner, BoardValue.X, boardMock);

        //when
        player.makeMove();

        //then
        verify(boardMock).move(0, 1, BoardValue.X);
    }

}
